# dnssec-signer

DNSSEC sign DNS zone(s) in one command. DNSSEC-ninja!

## Requirements

* dnssec
* haveged (optional, but absolutely a must have to speed up things!)

This script follows a special kind of structure, to more easily understand where to look up and read DNS zones, e.g:

```
/path/to/zones/<domain.tld>/
```

The zone file, without any DNSSEC entries, must be called `db` and be placed in the root of the folder `<domain.tld>/`. 

Personally this is an easier folder-structure to look-up and read when I'm working on a server, and the reason why I decided to have it like this.

## Installation

1. Clone this repository ```bash
git clone giaever@git.giaever.org:joachimmg/dnssec-signer.git && cd "dnssec-signer"
```

2. Make `dnssec-signer` executable ```bash
chmod +x dnssec-signer
```

3. Edit `dnssec-signer` and alter the configuration section:
	1. NAMEDZONES (default: `/etc/bind/named.conf.local`): The `named`-file with the zones you're authoritative for. This file is used to only sign active zones when arguments aren't given when executing the script. Typically used when running the script as a deamon.
	2. ZONESDIR (default: `/etc/bind/zones`): The `zones`-folder (see: Requirements) where you store your zones.
	3. CHECKZONE (default: `/usr/sbin/named-checkzone`): Where the application `named-checkzone` is located on in your OS.
	4. CHECKCONF (default: `/usr/sbin/named-checkconf`): Where the application `named-checkconf` is located on in your OS.
	5. KEYGEN (default: `/usr/sbin/dnssec-keygen`): Where the application `dnssec-keygen` is located on in your OS.
	6. SIGNZONE (default: `/usr/sbin/dnssec-signzone`): Where the application `dnssec-signzone` is located on in your OS.
	7. SYSCTL (default: `/bin/systemctl`): Where the application `systemctl` is located on in your OS.
	7. DNSSERVICE (default: `bind.service`): Which DNS service you're using and that `systemctl` will have to restart.
	8. LOGGER (default: `/usr/bin/logger`): Which logger application you will use and where it reside in your OS.
	9. LOGGERFLAGN (default: `-t "<username>" -p daemon.info`): Flags you want to pass to the `logger` when logging successful messages.
	9. LOGGERFLAGE (default: `-t "<username>" -p daemon.err`): Flags you want to pass to the `logger` when logging error messages.

If you like, you can link the file to a `bin`-directory, to globally access the script from any working directory.

```bash
ln -s /not/a/relative/path/to/dnssec-signer /usr/bin/
```
## Usage

A signed zone will be stored in a file called `db.signed` and is the one you have to refer to (or change) in the `named`-file.  This is something you only need to do once, after you signed the zone. 

Please dont delete the `db`-file!
1. You will do changes to a zone in this file (and not `db.signed`) and sign the zone again afterwards, which will generate a new `db.signed`-file for you. It's more or less impossible to do changes in the signed file.
2. If you're re-signing a zone it will always read the `db`-file and generate a new `db.signed`-file.

Please don't delete the **KEYS**! They are stored in the `<domain.tld>/`-directory, and they should be kept there for resigning.

The first time a zone is signed there is generated a file called `dsset-<domain.tld>`. To gain `the chain of trust` you must add these keys, in the `dsset`-file, with your registrar.

### Sign a single zone:
```bash
./dnssec-signer <zone>
```

### Sign several zones:
```bash
./dnssec-signer <zone> <zone> ... <zone>
```
Note: This won't require the zone(s) to be in the `named`-file

### Sign every "active" zone:
```bash
./dnssec-signer
```
Note: This will check that that zones is in your `named`-file before processing it, as
this is typically used in a cron-job to update the hash for the zone.

#### Create crontab?
Edit crontab for the root
```
sudo crontab -e
```
and add the following
```
0 0 */3 * * /path/to/dnssec-signer
```
This updates active zones every `3`'rd day. Append ` > /var/log/dnssec-signer` to store the last output log. Then you can open this and read the last result. There's also a timestamp produced at the start if you're wondering what time it updated the zone.

## Note
This is only tested under Bind9 on Ubuntu 16.04.03
